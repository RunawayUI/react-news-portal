import React from "react";
import { Routes, Route, Navigate } from "react-router-dom";

import MainPage, {routeMain as routeMainPage} from "pages/MainPage";
import Contacts, {routeMain as routeContacts} from "pages/Contacts";
import NewsListPage, {routeMain as routeNewsList} from "pages/NewsListPage";
import NewsDetail, {routeMain as routeNewsDetail} from "pages/NewsDetail";

import Header from "components/Header";
import Footer from "components/Footer";

import './style.scss';

const AppContent = () => {
  return (
    <div className="mainWrapper">
      <Header/>
        <main>
          <Routes>
            <Route path={routeMainPage()} element={<MainPage/>}/>
            <Route path={routeContacts()} element={<Contacts/>}/>
            <Route path={routeNewsList()} element={<NewsListPage/>}/>
            <Route path={routeNewsDetail()} element={<NewsDetail/>}/>
            <Route path='*' element={<Navigate to={routeMainPage()} replace/>}/>
          </Routes>
        </main>
      <Footer/>
    </div>
  );
}

export default AppContent;