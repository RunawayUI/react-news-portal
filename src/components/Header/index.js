import React from "react";
import { NavLink } from "react-router-dom";

import {routeMain as routeMainPage} from "pages/MainPage";
import {routeMain as routeContacts} from "pages/Contacts";
import {routeMain as routeNewsList} from "pages/NewsListPage";

import './style.scss'

const Header = () => {
  return (
    <div className="header">
      <div className="header__title">Новостник</div>
      <nav>
        <NavLink to={routeMainPage()} className={({isActive}) => isActive ? 'linkActive' : ''}>
          Главная   
        </NavLink>
        <NavLink to={routeNewsList()} className={({isActive}) => isActive ? 'linkActive' : ''}>
          Новости
        </NavLink>
        <NavLink to={routeContacts()} className={({isActive}) => isActive ? 'linkActive' : ''}>
          Контакты
        </NavLink>
      </nav>
    </div>
  );
}

export default Header;